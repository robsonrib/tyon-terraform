provider "aws" {
    region = "ap-southeast-2"
}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
  type = list(object({
    cidr_block = string
    name = string
  }))
}

resource "aws_vpc" "test-vpc" {
    cidr_block = var.vpc_cidr_block[0].cidr_block
    tags = {
      "Name" = var.vpc_cidr_block[0].name
          }
}

resource "aws_subnet" "test-subnet-1" {
    vpc_id = aws_vpc.test-vpc.id
    cidr_block = var.vpc_cidr_block[1].cidr_block
    availability_zone = "ap-southeast-2a"
    tags = {
      "Name" = var.vpc_cidr_block[1].name
    }
}

output "test-vpc-id" {
    value = aws_vpc.test-vpc.id
}

output "test-subnet-id" {
    value = aws_subnet.test-subnet-1.id
}